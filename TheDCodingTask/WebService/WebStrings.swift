//
//  WebStrings.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/15/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit

class WebStrings: NSObject {

    static let kBaseURL = "https://limitless-forest-98976.herokuapp.com"
    static let kMETHODGet = "GET"
    static let kHEADERContentTypeJSON = "application/json; charset=UTF-8"
    static let kHEADERContentType = "Content-type"
    static let kPARAMID = "id"
    static let kPARAMProductDescription = "productDescription"
    static let kPARAMImage = "image"
    static let kPARAMWidth = "width"
    static let kPARAMHeight = "height"
    static let kPARAMurl = "url"
    static let kPARAMPrice = "price"
    static let kPARAMCount = "count"
    static let kPARAMFrom = "from"
}
