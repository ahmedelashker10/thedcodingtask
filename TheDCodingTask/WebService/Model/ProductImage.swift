//
//  ProductImage.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/15/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class ProductImage: NSObject, NSCoding, Mappable {
    
    var width:Int!
    var height:Int!
    var url:String!
    
    required init?(map: Map) {
        
    }
    
    required init(coder aDecoder: NSCoder) {
        width = aDecoder.decodeObject(forKey: WebStrings.kPARAMWidth) as! Int
        height = aDecoder.decodeObject(forKey: WebStrings.kPARAMHeight) as! Int
        url = aDecoder.decodeObject(forKey: WebStrings.kPARAMurl) as! String
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(width, forKey: WebStrings.kPARAMWidth)
        aCoder.encode(height, forKey: WebStrings.kPARAMHeight)
        aCoder.encode(url, forKey: WebStrings.kPARAMurl)
    }
    
    func mapping(map: Map) {
        width <- map[WebStrings.kPARAMWidth]
        height <- map[WebStrings.kPARAMHeight]
        url <- map[WebStrings.kPARAMurl]
    }
}
