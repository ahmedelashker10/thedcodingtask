//
//  Product.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/15/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import ObjectMapper

class Product: NSObject, NSCoding, Mappable {
    
    var id:Int!
    var productDescription:String!
    var image:ProductImage!
    var price:Int!
    
    required init?(map: Map) {
        
    }
    
    required init(coder aDecoder: NSCoder) {
        id = aDecoder.decodeObject(forKey: WebStrings.kPARAMID) as! Int
        productDescription = aDecoder.decodeObject(forKey: WebStrings.kPARAMProductDescription) as! String
        image = aDecoder.decodeObject(forKey: WebStrings.kPARAMImage) as! ProductImage
        price = aDecoder.decodeObject(forKey: WebStrings.kPARAMPrice) as! Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: WebStrings.kPARAMID)
        aCoder.encode(productDescription, forKey: WebStrings.kPARAMProductDescription)
        aCoder.encode(image, forKey: WebStrings.kPARAMImage)
        aCoder.encode(price, forKey: WebStrings.kPARAMPrice)
    }
    
    func mapping(map: Map) {
        id <- map[WebStrings.kPARAMID]
        productDescription <- map[WebStrings.kPARAMProductDescription]
        image <- map[WebStrings.kPARAMImage]
        price <- map[WebStrings.kPARAMPrice]
    }
}
