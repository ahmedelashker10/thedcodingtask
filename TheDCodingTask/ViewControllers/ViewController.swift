//
//  ViewController.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/15/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import DataCache

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var count = 10
    var from = 1
    
    var collectionView:UICollectionView?
    var products:[Product] = []
    
    let kCELLREUSEProduct = "ProductCell"
    
    var lblTitle:UILabel?
    
    var activity = UIActivityIndicatorView(frame: CGRect(x: UIScreen.main.bounds.width/2 - 20,
                                                         y: UIScreen.main.bounds.height/2 - 20,
                                                         width: 40,
                                                         height: 40))
    
    let noInternetStr = "The Internet connection appears to be offline."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        addCollectionView()
        addTitleLabel()
        addActivity()
        requestProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func addActivity() {
        activity.activityIndicatorViewStyle = .whiteLarge
        activity.hidesWhenStopped = true
        activity.startAnimating()
        view.addSubview(activity)
        let horizontalConstraint = activity.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let verticalConstraint = activity.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
    }
    
    func addCollectionView() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: 150, height: 300)
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 8
        collectionView = UICollectionView(frame: UIScreen.main.bounds,
                                           collectionViewLayout: layout)
        
        collectionView?.register(ProductCell.self, forCellWithReuseIdentifier: kCELLREUSEProduct)
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.translatesAutoresizingMaskIntoConstraints = false
        collectionView?.contentInset = UIEdgeInsetsMake(72, 8, 8, 8)
        collectionView?.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 1, alpha: 1.0)
        view.addSubview(collectionView!)
        
        let horizontalConstraint = collectionView?.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let verticalConstraint = collectionView?.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        let widthConstraint = collectionView?.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0)
        let heightConstraint = collectionView?.heightAnchor.constraint(equalTo: view.heightAnchor, constant: -20)
        NSLayoutConstraint.activate([horizontalConstraint!, verticalConstraint!, widthConstraint!, heightConstraint!])
    }
    
    func addTitleLabel() {
        lblTitle = UILabel()
        lblTitle?.text = "Products"
        lblTitle?.sizeToFit()
        lblTitle?.textAlignment = .center
        lblTitle?.backgroundColor = UIColor.black
        lblTitle?.textColor = UIColor(red: 1, green: 62/255, blue: 150/255, alpha: 1.0)
        lblTitle?.font = UIFont.boldSystemFont(ofSize: 17)
        view.addSubview(lblTitle!)
        
        lblTitle?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 70)
        
        let widthConstraint = lblTitle?.widthAnchor.constraint(equalTo: view.widthAnchor)
        let topConstraint = lblTitle?.topAnchor.constraint(equalTo: view.topAnchor)
        let leftConstraint = lblTitle?.leftAnchor.constraint(equalTo: view.leftAnchor)
        let rightConstraint = lblTitle?.rightAnchor.constraint(equalTo: view.rightAnchor)
        NSLayoutConstraint.activate([widthConstraint!, topConstraint!, leftConstraint!, rightConstraint!])
    }
    
    func requestProducts() {
        Alamofire.request(URL(string:WebStrings.kBaseURL)!
            , method: .get
            , parameters: [WebStrings.kPARAMCount:count, WebStrings.kPARAMFrom:from]).responseArray { (response: DataResponse<[Product]>) in
                
                self.activity.stopAnimating()
                guard response.result.error == nil else {
                    if response.result.error?.localizedDescription == self.noInternetStr {
                        self.readFromCache()
                    }
                    else {
                        self.showAlert(msg: (response.result.error?.localizedDescription)!)
                    }
                    return
                }
                
                let productsArray = response.result.value
                self.products.append(contentsOf: productsArray!)
                self.cacheProducts(productsArray: productsArray!)
                self.from = (productsArray?.last?.id)! + 1
                self.collectionView?.reloadData()
        }
    }
    
    func cacheProducts(productsArray:[Product]) {
        for product in productsArray {
            let stringID = String(product.id)
            DataCache.instance.write(object: product, forKey: stringID)
        }
    }
    
    func readFromCache() {
        var productsArray:[Product] = []
        for i in from..<from+count {
            let stringID = String(i)
            if let cachedProduct = DataCache.instance.readObject(forKey: stringID) as? Product {
                productsArray.append(cachedProduct)
            } else {
                break
            }
        }
        
        if productsArray.count == count {
            products.append(contentsOf: productsArray)
            from = (productsArray.last?.id)! + 1
            collectionView?.reloadData()
        }
        else {
            showAlert(msg: noInternetStr)
        }
    }
    
    func showAlert(msg:String) {
        let alertController = UIAlertController(title: "TheD",
                                                message: msg,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: Collection View Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: kCELLREUSEProduct, for: indexPath) as! ProductCell
        
        let product = products[indexPath.item]
        cell.price = String(product.price)
        cell.imgUrl = product.image.url
        cell.imgWidth = product.image.width
        cell.imgHeight = product.image.height
        cell.descriptionText = product.productDescription
        cell.addChilds()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let product = products[indexPath.item]
        let cellWidth = UIScreen.main.bounds.width / 2 - 16
        let cellHeight = (cellWidth / CGFloat(product.image.width)) * CGFloat(product.image.height) // Scalability
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let product = products[indexPath.item]
        let pdvc = ProductDetailsViewController()
        pdvc.product = product
        navigationController?.pushViewController(pdvc, animated: true)
    }
    
    //MARK: Load More
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
        if (bottomEdge >= scrollView.contentSize.height - 1) {
            // we are at the end
            requestProducts()
        }
    }
}
