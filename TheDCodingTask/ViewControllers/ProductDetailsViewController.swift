//
//  ProductDetailsViewController.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/17/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import SDWebImage

class ProductDetailsViewController: UIViewController {
    
    var product:Product?
    
    private var lblPrice: UILabel = UILabel()
    private var imgView: UIImageView = UIImageView()
    private var lblDescription: UILabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.tintColor = UIColor(red: 1, green: 62/255, blue: 150/255, alpha: 1.0)
        view.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 1, alpha: 1.0)
        
        addImageView()
        addPrice()
        addDescription()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func addImageView() {
        imgView = UIImageView(frame: CGRect(x: (Int(view.frame.width) / 2) - (product!.image.width / 2),
                                            y: 80,
                                            width: (product?.image.width!)!,
                                            height: (product?.image.height!)!))
        imgView.sd_setImage(with: URL(string: (product?.image.url)!))
        imgView.contentMode = .scaleToFill
        view.addSubview(imgView)
        
        let widthConstraint = imgView.widthAnchor.constraint(equalToConstant: CGFloat((product?.image.width!)!))
        let heightConstraint = imgView.heightAnchor.constraint(equalToConstant: CGFloat((product?.image.height!)!))
        let horizontalConstraint = imgView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let topConstraint = imgView.topAnchor.constraint(equalTo: view.topAnchor, constant: 80)
        NSLayoutConstraint.activate([widthConstraint, heightConstraint, horizontalConstraint, topConstraint])
    }
    
    private func addPrice() {
        lblPrice.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        lblPrice.text = String(product!.price) + "$"
        lblPrice.sizeToFit()
        lblPrice.frame = CGRect(x: CGFloat((Int(view.frame.width) / 2) - (Int(lblPrice.frame.width) / 2) - 5),
                                y: CGFloat(80 + (product?.image.height!)! + 8),
                                width: (lblPrice.frame.width) + 10,
                                height: (lblPrice.frame.height))
        lblPrice.textAlignment = .center
        view.addSubview(lblPrice)
        
        let horizontalConstraint = lblPrice.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let topConstraint = lblPrice.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 8)
        NSLayoutConstraint.activate([horizontalConstraint, topConstraint])
    }
    
    private func addDescription() {
        lblDescription.numberOfLines = 0
        lblDescription.backgroundColor = UIColor.white.withAlphaComponent(0.7)
        lblDescription.text = product?.productDescription
        lblDescription.sizeToFit()
        lblDescription.textAlignment = .center
        
        let lblSize = lblDescription.sizeThatFits(CGSize(width: view.frame.width-16, height: view.frame.height))
        lblDescription.frame = CGRect(x: 0,
                                      y: 0,
                                      width: view.frame.width,
                                      height: (lblSize.height))
        
        let scroll:UIScrollView = UIScrollView(frame: CGRect(x: 8,
                                                             y: lblPrice.frame.origin.y + 60,
                                                             width: view.frame.width - 16,
                                                             height: view.frame.height - lblPrice.frame.origin.y - 60))
        scroll.addSubview(lblDescription)
        scroll.contentSize = lblSize
        view.addSubview(scroll)
        
        let bottomConstraint = scroll.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        let rightConstraint = scroll.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0)
        let leftConstraint = scroll.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0)
        NSLayoutConstraint.activate([bottomConstraint, rightConstraint, leftConstraint])
    }
}
