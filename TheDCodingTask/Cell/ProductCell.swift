//
//  ProductCell.swift
//  TheDCodingTask
//
//  Created by Ahmed Elashker on 3/16/17.
//  Copyright © 2017 Ahmed Elashker. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCell: UICollectionViewCell {
    
    var price:String?
    var imgUrl:String?
    var imgWidth:Int?
    var imgHeight:Int?
    var descriptionText:String?
    
    private var lblPrice: UILabel = UILabel()
    private var imgView: UIImageView = UIImageView()
    private var lblDescription: UILabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        for subview in subviews {
            subview.removeFromSuperview()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func addChilds() {
        addImageView()
        addPrice()
        addDescription()
    }
    
    private func addImageView() {
        
        imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: Int(self.frame.width), height: imgHeight!))
        imgView.sd_setImage(with: URL(string: imgUrl!), completed: { _ in
            self.addPrice()
            self.addDescription()
        })
        imgView.contentMode = .scaleToFill
        self.addSubview(imgView)
        
        let horizontalConstraint = imgView.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let verticalConstraint = imgView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        let widthConstraint = imgView.widthAnchor.constraint(equalTo: self.widthAnchor)
        let heightConstraint = imgView.heightAnchor.constraint(equalTo: self.heightAnchor)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
    
    private func addPrice() {
        
        lblPrice.backgroundColor = UIColor.white
        lblPrice.text = price! + "$"
        lblPrice.sizeToFit()
        lblPrice.frame = CGRect(x: self.frame.width - 16 - (lblPrice.frame.width),
                                 y: 8,
                                 width: (lblPrice.frame.width) + 10,
                                 height: (lblPrice.frame.height))
        lblPrice.textAlignment = .center
        self.addSubview(lblPrice)
        
        let topConstraint = lblPrice.topAnchor.constraint(equalTo: self.topAnchor, constant: 8)
        let rightConstraint = lblPrice.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8)
        NSLayoutConstraint.activate([topConstraint, rightConstraint])
    }
    
    private func addDescription() {
        
        lblDescription.numberOfLines = 0
        lblDescription.backgroundColor = UIColor.white
        lblDescription.text = descriptionText
        lblDescription.sizeToFit()
        
        let lblSize = lblDescription.sizeThatFits(CGSize(width: self.frame.width, height: self.frame.height))
        lblDescription.frame = CGRect(x: 0, y: self.frame.height-(lblSize.height), width: self.frame.width, height: (lblSize.height))
        self.addSubview(lblDescription)
        
        let bottomConstraint = lblDescription.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0)
        let rightConstraint = lblDescription.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0)
        let leftConstraint = lblDescription.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0)
        NSLayoutConstraint.activate([bottomConstraint, rightConstraint, leftConstraint])
    }
}
